use slice_deque::SliceDeque;

fn main() {
    let mut deque = SliceDeque::new();

    loop {
        deque.push_back(String::from("test"));
        if deque.len() == 8 {
            deque.pop_front();
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        super::main()
    }
}
