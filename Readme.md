# SliceDeque bug reprduction

Steps to reproduce

```shell
cargo run
```

I get an output like this within moments of running:

```
mate3(80333,0x11215c5c0) malloc: *** error for object 0x4: pointer being freed was not allocated
mate3(80333,0x11215c5c0) malloc: *** set a breakpoint in malloc_error_break to debug
'cargo run' terminated by signal SIGABRT (Abort)
```
